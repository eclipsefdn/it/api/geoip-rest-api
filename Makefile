SHELL = /bin/bash

pre-setup:;
	@echo "Creating environment file from template"
	@rm -f .env && envsubst < config/.env.sample > .env
setup:;
	@source .env && bash ./bin/maxmind.sh maxmind/
dev-start:;
	mvn compile quarkus:dev

clean:;
	mvn clean

compile-test-resources: install-yarn;
	yarn run generate-json-schema

install-yarn:;
	yarn install --frozen-lockfile

compile-java: compile-test-resources;
	mvn compile package -Declipse.maxmind.root=${PWD}/maxmind

compile-java-quick: compile-test-resources;
	mvn compile package -Dmaven.test.skip=true -Declipse.maxmind.root=${PWD}/maxmind

compile: clean compile-java;

compile-quick: clean compile-java-quick;

compile-start: compile-quick;
	docker compose down
	docker compose build
	docker compose up -d