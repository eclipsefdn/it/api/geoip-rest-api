/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.geoip.client.service.impl;

import java.util.List;

import org.eclipsefoundation.geoip.client.model.IPVersion;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;

/**
 * Basic tests for CSV network service to ensure that case insensitivity is observed.
 */
@QuarkusTest
class CSVNetworkServiceTest {

    @Inject
    CSVNetworkService svc;

    @Test
    void getSubnets_success() {
        List<String> subnets = svc.getSubnets("ca", IPVersion.IPV4);
        Assertions.assertTrue(!subnets.isEmpty());
    }
    
    // Iss #35 test
    @Test
    void getSubnets_success_caseInsensitive() {
        List<String> subnets = svc.getSubnets("CA", IPVersion.IPV4);
        Assertions.assertTrue(!subnets.isEmpty());
    }
}
