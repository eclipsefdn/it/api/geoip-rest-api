/*********************************************************************
* Copyright (c) 2019 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.geoip.client.resources;

import org.eclipsefoundation.geoip.client.test.namespaces.SchemaNamespaceHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;

import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

/**
 * Tests for the Subnets resource, which checks and promises the given endpoints
 * with some parameter checking.
 * 
 * @author Martin Lowe
 *
 */
@QuarkusTest
class SubnetResourceTest {
	public static final String SUBNETS_ENDPOINT_URL = "/subnets/{subnet}/{locale}";

    public static final EndpointTestCase IPV4_SUCCESS = TestCaseHelper.buildSuccessCase(SUBNETS_ENDPOINT_URL,
            new String[] { "ipv4", "ca" }, SchemaNamespaceHelper.IP_ADDRESSES_SCHEMA_PATH);

	public static final EndpointTestCase IPV6_SUCCESS = TestCaseHelper.buildSuccessCase(SUBNETS_ENDPOINT_URL,
			new String[] { "ipv6", "ca" }, SchemaNamespaceHelper.IP_ADDRESSES_SCHEMA_PATH);

	@Test
	void testSubnets_success() {
		EndpointTestBuilder.from(IPV4_SUCCESS).run();
		EndpointTestBuilder.from(IPV6_SUCCESS).run();
	}

	@Test
	void testSubnets_success_validSchema() {
		EndpointTestBuilder.from(IPV4_SUCCESS).andCheckSchema().run();
		EndpointTestBuilder.from(IPV6_SUCCESS).andCheckSchema().run();
	}

	@Test
	void testSubnets_success_validateResponseFormat() {
		EndpointTestBuilder.from(IPV4_SUCCESS).andCheckFormat().run();
		EndpointTestBuilder.from(IPV6_SUCCESS).andCheckFormat().run();
	}

	@Test
	void testSubnets_failure_invalidLocales() {
		EndpointTestCase testCase;
		String[] badLocales = new String[] { "", "can", "01" };

		// bad ipv4 calls
		for (String locale : badLocales) {
			testCase = TestCaseHelper.buildNotFoundCase(SUBNETS_ENDPOINT_URL, new String[] { "ipv4", locale }, "");
			EndpointTestBuilder.from(testCase).run();
		}

		// bad ipv6 calls
		for (String locale : badLocales) {
			testCase = TestCaseHelper.buildNotFoundCase(SUBNETS_ENDPOINT_URL, new String[] { "ipv6", locale }, "");
			EndpointTestBuilder.from(testCase).run();
		}
	}

	@Test
	void testSubnets_failure_invalidSubnets() {
		EndpointTestCase testCase;
		String[] badSubnets = new String[] { "ipv5", "ipvfour", "ipv" };

		// check other permutations (regex endpoint)
		for (String subnet : badSubnets) {
			testCase = TestCaseHelper.buildNotFoundCase(SUBNETS_ENDPOINT_URL, new String[] { subnet, "ca" }, "");
			EndpointTestBuilder.from(testCase).run();
		}
	}
}
