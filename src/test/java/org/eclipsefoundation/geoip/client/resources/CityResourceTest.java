/*********************************************************************
* Copyright (c) 2019 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.geoip.client.resources;

import org.eclipsefoundation.geoip.client.test.namespaces.SchemaNamespaceHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;

import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

/**
 * Test the listing resource endpoint, using fake data points to test solely the
 * responsiveness of the endpoint.
 * 
 * @author Martin Lowe
 */
@QuarkusTest
class CityResourceTest {
	public static final String CITIES_ENDPOINT_URL = "/cities/{ipAddr}";

	// Toronto IP address range
	private static final String VALID_IPV4_ADDRESS = "72.137.192.0";
	// Google IE server address
	private static final String VALID_IPV6_ADDRESS = "2a00:1450:400a:804::2004";

	public static final EndpointTestCase IPV4_SUCCESS = TestCaseHelper.buildSuccessCase(CITIES_ENDPOINT_URL,
			new String[] { VALID_IPV4_ADDRESS }, SchemaNamespaceHelper.CITY_SCHEMA_PATH);

	public static final EndpointTestCase IPV6_SUCCESS = TestCaseHelper.buildSuccessCase(CITIES_ENDPOINT_URL,
			new String[] { VALID_IPV6_ADDRESS }, SchemaNamespaceHelper.CITY_SCHEMA_PATH);

	@Test
	void testCities_success() {
		EndpointTestBuilder.from(IPV4_SUCCESS).run();
	}

	@Test
	void testCities_success_validSchema() {
		EndpointTestBuilder.from(IPV4_SUCCESS).andCheckSchema().run();
	}

	@Test
	void testCities_success_validateResponseFormat() {
		EndpointTestBuilder.from(IPV4_SUCCESS).andCheckFormat().run();
	}

	@Test
	void testCity_failure_badIPs() {

		EndpointTestCase testCase;

		String[] badIPV4s = new String[] { "bad.ip.add.res", "300.0.0.0", "1.300.0.0", "1.0.300.0", "1.0.0.300",
				"sample", VALID_IPV4_ADDRESS + ":8080" };

		// IPv4 tests
		for (String ip : badIPV4s) {
			testCase = TestCaseHelper.buildBadRequestCase(CITIES_ENDPOINT_URL, new String[] { ip }, "");
			EndpointTestBuilder.from(testCase).run();
		}

		// IPv6 test
		testCase = TestCaseHelper.buildBadRequestCase(CITIES_ENDPOINT_URL, new String[] { "bad:ip:add::res" }, "");
		EndpointTestBuilder.from(testCase).run();
	}

	@Test
	void testCities_failure_loopback() {
		EndpointTestCase testCase;
		// loopback + unspecified address
		String[] badIPV4s = new String[] { "127.0.0.1", "0.0.0.0" };
		String[] badIPV6s = new String[] { "::", "::1" };

		for (String ip : badIPV4s) {
			testCase = TestCaseHelper.buildBadRequestCase(CITIES_ENDPOINT_URL, new String[] { ip }, "");
			EndpointTestBuilder.from(testCase).run();
		}

		for (String ip : badIPV6s) {
			testCase = TestCaseHelper.buildBadRequestCase(CITIES_ENDPOINT_URL, new String[] { ip }, "");
			EndpointTestBuilder.from(testCase).run();
		}
	}
}
