/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.geoip.client.resources;

import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.geoip.client.service.GeoIPService;
import org.eclipsefoundation.http.exception.ApplicationException;

import com.google.common.net.InetAddresses;
import com.maxmind.geoip2.record.AbstractNamedRecord;

import jakarta.inject.Inject;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.core.Response;

/**
 * Superclass of {@link CountryResource} and {@link CityResource}. Contains a
 * method for converting a City/Country to Json, as well as validation for
 * incoming IP addresses.
 */
public abstract class AbstractLocationResource {

    private static final List<String> INVALID_ADDRESSES_IPv4 = List.of("127.0.0.1", "0.0.0.0");
    private static final List<String> INVALID_ADDRESSES_IPv6 = List.of("::1", "::");

    @Inject
    GeoIPService geoIp;

    /**
     * Returns a 200 OK Response containing the JSON converted entity. Converts to
     * JSON using the built-in {@link AbstractNamedRecord#toJson()} Throws a 500
     * Internal Server Error if there was an issue converting the body to JSON.
     * 
     * @param record The entity to convert and send to the client
     * @return A 200 OK Response containing the JSON converted entity. A 500
     *         Internal Server Error if there was an issue converting the body to
     *         JSON.
     */
    protected Response returnAsJson(AbstractNamedRecord result) {
        // Return converted country/city
        try {
            return Response.ok(result.toJson()).build();
        } catch (IOException e) {
            throw new ApplicationException("Error while converting country record to JSON", e);
        }
    }

    /**
     * Validates the incoming IP address. IPs are invalid if they are null,
     * contained in the list of invalid Ipv4/Ipv6 addresses, or considered invalid
     * by {@link InetAddresses#isInetAddress(String)}. Throws a 400
     * BadRequestException if the Guava check throws an error validating the IP.
     * 
     * @param address The given IP address to validate.
     * @return True if the IP is valid. False if not.
     */
    protected boolean isValidInetAddress(String address) {
        try {
            // Check null and lightweight options first for invalid addresses
            if (StringUtils.isBlank(address)
                    || INVALID_ADDRESSES_IPv4.contains(address)
                    || INVALID_ADDRESSES_IPv6.contains(address)) {
                return false;
            }

            // Use Google Guava as larger more intensive check of string values
            return InetAddresses.isInetAddress(address);
        } catch (Exception e) {
            throw new BadRequestException("Passed IP address was not a valid address");
        }
    }
}
