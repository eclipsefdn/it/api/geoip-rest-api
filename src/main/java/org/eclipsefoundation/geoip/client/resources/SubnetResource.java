/*********************************************************************
* Copyright (c) 2019 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.geoip.client.resources;

import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import org.eclipsefoundation.geoip.client.model.IPVersion;
import org.eclipsefoundation.geoip.client.service.NetworkService;
import org.eclipsefoundation.http.namespace.CacheControlCommonValues;
import org.jboss.resteasy.reactive.Cache;

/**
 * Provides subnets for given country codes, in both ipv4 and ipv6 formats upon
 * request.
 * 
 * @author Martin Lowe
 */
@Path("/subnets")
@Produces(MediaType.APPLICATION_JSON)
@Cache(maxAge = CacheControlCommonValues.AGGRESSIVE_CACHE_MAX_AGE)
public class SubnetResource {

	@Inject
	NetworkService networks;

	@GET
	@Path("/{ipv: (ipv[46])}/{isoLocale: ([a-zA-Z]{2})}")
	public Response get(@PathParam("isoLocale") String isoLocale, @PathParam("ipv") String ipVersion) {
		return Response.ok(networks.getSubnets(isoLocale, IPVersion.getByName(ipVersion))).build();
	}
}
