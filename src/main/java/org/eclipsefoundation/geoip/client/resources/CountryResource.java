/*********************************************************************
* Copyright (c) 2019, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*			Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.geoip.client.resources;

import org.eclipsefoundation.http.namespace.CacheControlCommonValues;
import org.jboss.resteasy.reactive.Cache;

import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/**
 * Main resource class for all endpoints under '/countries'.
 */
@Path("/countries")
@Produces(MediaType.APPLICATION_JSON)
@Cache(maxAge = CacheControlCommonValues.AGGRESSIVE_CACHE_MAX_AGE)
public class CountryResource extends AbstractLocationResource {
    
	/**
	 * Returns a 200 OK Response containing the country location of a given IP
	 * address. Returns a 400 Bad Request if the IP is invalid. Returns a 500
	 * Internal Server Error if there is an issue retrieving the IP location, or if
	 * the outgoing JSON conversion fails.
	 * 
	 * @param ipAddr The IP address to search.
	 * @return A 200 OK Response containing the country location of a given IP
	 *         address. A 400 Bad Request if the IP is invalid. A 500 Internal
	 *         Server Error if there is an issue retrieving the IP location, or if
	 *         the outgoing JSON conversion fails.
	 */
	@GET
	@Path("/{ipAddr}")
	public Response get(@PathParam("ipAddr") String ipAddr) {
		// validate IP
		if (!isValidInetAddress(ipAddr)) {
			throw new BadRequestException("Valid IP address must be passed to retrieve location data");
		}

		// Retrieve cached country data and return the JSON object
		return returnAsJson(geoIp.getCountry(ipAddr));
	}
}
