/*********************************************************************
* Copyright (c) 2019, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*		Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.geoip.client.service.impl;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.util.Optional;

import org.eclipsefoundation.geoip.client.config.MaxmindDatabaseConfig;
import org.eclipsefoundation.geoip.client.service.GeoIPService;
import org.eclipsefoundation.http.exception.ApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.AddressNotFoundException;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.model.CountryResponse;
import com.maxmind.geoip2.record.City;
import com.maxmind.geoip2.record.Country;

import io.quarkus.runtime.Startup;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.enterprise.context.ApplicationScoped;

/**
 * Implementation of the Geo-IP service using MaxMind. Built in caching is ignored as its a very light implementation that has no eviction
 * policy and silently stops using cache if it gets full.
 * 
 * @author Martin Lowe
 */
@Startup
@ApplicationScoped
public class MaxMindGeoIPService implements GeoIPService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MaxMindGeoIPService.class);

    private MaxmindDatabaseConfig config;
    private DatabaseReader countryReader;
    private DatabaseReader cityReader;

    public MaxMindGeoIPService(MaxmindDatabaseConfig config) {
        this.config = config;
    }

    /**
     * Create instances of DB readers for each available DB
     */
    @PostConstruct
    public void init() {
        File countryDb = new File(config.dbRoot() + File.separator + config.countryFileName());
        File cityDb = new File(config.dbRoot() + File.separator + config.cityFileName());
        // attempt to load db's, throwing up if there is an issue initializing the
        // readers
        try {
            this.countryReader = new DatabaseReader.Builder(countryDb).build();
            this.cityReader = new DatabaseReader.Builder(cityDb).build();
        } catch (IOException e) {
            throw new ApplicationException("Error pre-loading Maxmind DatabaseReaders", e);
        }
    }

    @PreDestroy
    public void destroy() {
        try {
            this.countryReader.close();
            this.cityReader.close();
        } catch (IOException e) {
            LOGGER.error("Error while closing Geo IP databases, potential memory leak", e);
        }
    }

    @Override
    public Optional<City> getCity(String ipAddr) {
        try {
            InetAddress addr = InetAddress.getByName(ipAddr);
            CityResponse resp = cityReader.city(addr);
            return Optional.of(resp.getCity());
        } catch (AddressNotFoundException e) {
            LOGGER.trace("IP address '{}' does not have an associated city", ipAddr);
            return Optional.empty();
        } catch (Exception e) {
            throw new ApplicationException("Error interacting with GeoIP databases", e);
        }
    }

    @Override
    public Country getCountry(String ipAddr) {
        try {
            InetAddress addr = InetAddress.getByName(ipAddr);
            CountryResponse resp = countryReader.country(addr);
            return resp.getCountry();
        } catch (Exception e) {
            throw new ApplicationException("Error interacting with GeoIP databases", e);
        }
    }
}
