/*********************************************************************
* Copyright (c) 2019, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*			Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.geoip.client.service;

import java.util.Optional;

import com.maxmind.geoip2.record.City;
import com.maxmind.geoip2.record.Country;

/**
 * Defines the GeoIPService used to fetch city and country location data via IP
 * address.
 */
public interface GeoIPService {

	/**
	 * Returns the city where the given IP address is located.
	 * 
	 * @param ipAddr The given IP address
	 * @return A City entity corresponding to the IP address' location.
	 */
	Optional<City> getCity(String ipAddr);

	/**
	 * Returns the country where the given IP address is located.
	 * 
	 * @param ipAddr The given IP address
	 * @return A Country entity corresponding to the IP address' location.
	 */
	Country getCountry(String ipAddr);
}
