/*********************************************************************
* Copyright (c) 2019 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.geoip.client.service.impl;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.eclipsefoundation.geoip.client.config.EclipseMaxmindSubnetConfig;
import org.eclipsefoundation.geoip.client.model.Country;
import org.eclipsefoundation.geoip.client.model.IPVersion;
import org.eclipsefoundation.geoip.client.model.SubnetRange;
import org.eclipsefoundation.geoip.client.service.NetworkService;
import org.eclipsefoundation.http.exception.ApplicationException;

import com.opencsv.bean.CsvToBeanBuilder;

import io.quarkus.runtime.Startup;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;

/**
 * Loads Network subnet information via CSV files on the system.
 * 
 * @author Martin Lowe
 *
 */
@Startup
@ApplicationScoped
public class CSVNetworkService implements NetworkService {

    private EclipseMaxmindSubnetConfig config;

    public CSVNetworkService(EclipseMaxmindSubnetConfig config) {
        this.config = config;
    }

    private Map<String, String> countryIdToIso;
    private Map<String, List<String>> ipv4Subnets;
    private Map<String, List<String>> ipv6Subnets;

    @PostConstruct
    public void init() {
        this.ipv4Subnets = new HashMap<>();
        this.ipv6Subnets = new HashMap<>();
        this.countryIdToIso = new HashMap<>();
        loadCountries(config.countriesFilePath(), countryIdToIso);
        loadMap(config.ipv4FilePath(), ipv4Subnets);
        // Pre-loading ipv6 entries is a 2million item list and crashes the app
    }

    @Override
    public List<String> getSubnets(String countryCode, IPVersion ipv) {
        Objects.requireNonNull(countryCode);
        // Iss #35, unnormalized country codes won't always match when expected
        String normalizedCountryCode = countryCode.toLowerCase();
        // match on ipversion, and check that we have value. If found, return copy, else return empty list
        if (IPVersion.IPV4.equals(ipv) && ipv4Subnets.containsKey(normalizedCountryCode)) {
            return new ArrayList<>(ipv4Subnets.get(normalizedCountryCode));
        } else if (IPVersion.IPV6.equals(ipv) && ipv6Subnets.containsKey(normalizedCountryCode)) {
            return new ArrayList<>(ipv6Subnets.get(normalizedCountryCode));
        }
        return Collections.emptyList();
    }

    /**
     * Reads all Country entities from a desired CSV file and loads them into the desired container.
     * 
     * @param filePath The location of the CSV file to read.
     * @param container Reference to the Map to store all entries read from the file.
     */
    private void loadCountries(String filePath, Map<String, String> container) {
        try (FileReader reader = new FileReader(filePath)) {
            // read in all of the country lines as country objects
            List<Country> countries = new CsvToBeanBuilder<Country>(reader).withType(Country.class).build().parse();
            // add each of the countries to the container map, mapping ID to the ISO code
            for (Country c : countries) {
                container.put(c.getId(), c.getCountryIsoCode().toLowerCase());
            }
        } catch (FileNotFoundException e) {
            throw new ApplicationException("Could not find country CSV file at path " + filePath, e);
        } catch (IOException e) {
            throw new ApplicationException("Error while reading in CSV file at path " + filePath, e);
        }
    }

    /**
     * Reads all Subnet entities from a desired CSV file and loads them into the desired container.
     * 
     * @param filePath The location of the CSV file to read.
     * @param container Reference to the Map to store all entries read from the file.
     */
    private void loadMap(String filePath, Map<String, List<String>> container) {
        try (FileReader reader = new FileReader(filePath)) {
            // read in all of the country lines as country objects
            List<SubnetRange> subnets = new CsvToBeanBuilder<SubnetRange>(reader).withType(SubnetRange.class).build().parse();
            for (SubnetRange subnet : subnets) {
                // for each of the subnet ranges, get the best available ID
                String actualId = subnet.getGeoname() != null ? subnet.getGeoname() : subnet.getRegisteredGeoId();
                // lookup the ISO code for the current entry
                String isoCode = countryIdToIso.get(actualId);
                // if exists, add to subnet range list in map
                if (isoCode != null) {
                    container.computeIfAbsent(isoCode, k -> new ArrayList<>()).add(subnet.getNetwork());
                }
            }
        } catch (FileNotFoundException e) {
            throw new ApplicationException("Could not find country CSV file at path " + filePath, e);
        } catch (IOException e) {
            throw new ApplicationException("Error while reading in CSV file at path " + filePath, e);
        }
    }
}
