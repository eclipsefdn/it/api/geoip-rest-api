/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.geoip.client.config;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithName;

/**
 * Config mapper for maxmind database configs. Contains the db root folder, as
 * well as city and country file names.
 */
@ConfigMapping(prefix = "maxmind.database")
public interface MaxmindDatabaseConfig {

    @WithName("root")
    String dbRoot();

    @WithName("city.file")
    String cityFileName();

    @WithName("country.file")
    String countryFileName();
}
