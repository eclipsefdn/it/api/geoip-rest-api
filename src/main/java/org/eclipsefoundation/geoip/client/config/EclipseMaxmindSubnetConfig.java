/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.geoip.client.config;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;

/**
 * Config mapper for eclipse maxmind file locations. Contains filepaths for
 * ipv4, ipv6, and countries files.
 */
@ConfigMapping(prefix = "eclipse.maxmind.subnet")
public interface EclipseMaxmindSubnetConfig {

    @WithDefault(value = "/tmp/maxmind/db/GeoLite2-Country-Blocks-IPv4.csv")
    String ipv4FilePath();

    @WithDefault(value = "/tmp/maxmind/db/GeoLite2-Country-Blocks-IPv6.csv")
    String ipv6FilePath();

    @WithDefault(value = "/tmp/maxmind/db/GeoLite2-Country-Locations-en.csv")
    String countriesFilePath();
}
